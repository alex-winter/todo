import Vue from 'vue'
import Router from 'vue-router'
import JobList from '../views/JobList'
import JobPage from '../views/JobPage'
import LoginPage from '../views/Login'
import RegisterPage from '../views/Register'
import JobCreatePage from '../views/JobCreate'
import Invite from '../views/Invite'

Vue.use(Router);

const landingPage = function (to, from, next) {
    if (localStorage.getItem('user-token')) {
        next('/user/jobs');
    }

    next('/login');
};

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'landing-page',
            beforeEnter: landingPage
        },
        {
            path: '/user/jobs',
            name: 'user-jobs',
            component: JobList,
        },
        {
            path: '/job/create',
            name: 'job-create',
            component: JobCreatePage,
        },
        {
            path: '/job/:jobReference',
            name: 'job-view',
            props: true,
            component: JobPage,
        },
        {
            path: '/login',
            name: 'login',
            component: LoginPage,
        },
        {
            path: '/register',
            name: 'register',
            component: RegisterPage,
        },
        {
            path: '/invite',
            name: 'invite',
            component: Invite,
        },
    ]
});
