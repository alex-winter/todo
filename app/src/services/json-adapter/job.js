import Job from '../../model/job';
import User from '../../model/user';

const userAdapter = function (user) {
    return new User(user.reference, user.username);
};

export default function (json) {
    return Job.make(
        json.reference,
        json.name,
        json.description,
        userAdapter(json.createdBy),
        json.assignees.map(userAdapter),
        json.status
    );
};
