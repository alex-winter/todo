export default {

    make: function (
        reference,
        name,
        description,
        createdBy,
        assignees,
        status
    ) {
        switch (status) {
            case 0:
                status = 'Archived';
                break;

            case 1:
                status = 'Open';
                break;
        }

        return {
            reference,
            name,
            description,
            createdBy,
            assignees,
            status
        }
    },

    STATUS_ARCHIVED: 'Archived',
    STATUS_OPEN: 'Open',
};
