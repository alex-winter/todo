# API

### Prerequisites
* Docker with docker compose [download here](https://www.docker.com/get-started)
* git VCS [download here](https://git-scm.com/downloads)

### Optional Prerequisites
* Postman to test api requests [download here](https://www.getpostman.com/)

### API documentation
[click here](https://documenter.getpostman.com/view/2221327/SVSDPqvX?version=latest)

### Setup
The development environment is encapsulated in a containerized system, simply run the following to create the environment: 
```
docker-compose up
```

### Seed database with mock data
```
docker-compose exec app composer seed
```

### Unit Tests
```
docker-compose exec app composer tests:unit
```
