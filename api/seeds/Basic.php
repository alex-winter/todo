<?php

use Phinx\Seed\AbstractSeed;

class Basic extends AbstractSeed
{
    public function run(): void
    {
        $users = [
            [
                'id'        => 1,
                'reference' => '3432ce2e-c95e-4558-b9f2-c5e1f49f34e3',
            ],
            [
                'id'        => 2,
                'reference' => '9321bbcb-094c-47a8-9180-95daffac287e',
            ],
            [
                'id'        => 3,
                'reference' => '33c694b5-81de-4a1d-ae9e-2b95989a2984',
            ],
            [
                'id'        => 4,
                'reference' => '6cf0548d-b0d6-4066-b296-9e9abba3dfbf',
            ],
        ];

        $authentication = [
            [
                'id'       => 1,
                'user_id'  => 1,
                'username' => 'alex',
                'password' => password_hash('alex', PASSWORD_BCRYPT),
            ],
            [
                'id'       => 2,
                'user_id'  => 2,
                'username' => 'lewis',
                'password' => password_hash('lewis', PASSWORD_BCRYPT),
            ],
            [
                'id'       => 3,
                'user_id'  => 3,
                'username' => 'maria',
                'password' => password_hash('maria', PASSWORD_BCRYPT),
            ],
            [
                'id'       => 4,
                'user_id'  => 4,
                'username' => 'adrian',
                'password' => password_hash('adrian', PASSWORD_BCRYPT),
            ],
        ];

        $jobs = [
            [
                'id'            => 1,
                'created_by_id' => 1,
                'name'          => 'Job A',
                'description'   => 'job a descriptions',
                'status'        => 1,
                'reference'     => 'c08d26e8-7c73-4598-aa3d-614ab48ec3c9',
            ],
            [
                'id'            => 2,
                'created_by_id' => 1,
                'name'          => 'Job B',
                'description'   => 'job b descriptions',
                'status'        => 0,
                'reference'     => 'e528d6c1-084a-4e1e-8778-423c546eeab3',
            ],
            [
                'id'            => 3,
                'created_by_id' => 1,
                'name'          => 'Job C',
                'description'   => 'job c descriptions',
                'status'        => 1,
                'reference'     => 'a9155afa-e061-465a-aa8a-2fe7b8eeb1bc',
            ],
            [
                'id'            => 4,
                'created_by_id' => 2,
                'name'          => 'Job D',
                'description'   => 'job d descriptions',
                'status'        => 0,
                'reference'     => 'f6dd9e09-9560-4d63-ae14-eab4c7c8a1fb',
            ],
            [
                'id'            => 5,
                'created_by_id' => 2,
                'name'          => 'Job E',
                'description'   => 'job e descriptions',
                'status'        => 2,
                'reference'     => 'edc2f446-7283-4ab3-9281-155bd6d27497',
            ],
            [
                'id'            => 6,
                'created_by_id' => 3,
                'name'          => 'Job F',
                'description'   => 'job f descriptions',
                'status'        => 1,
                'reference'     => '094dc307-dce5-4b76-b9f4-2b06d6967909',
            ],
            [
                'id'            => 7,
                'created_by_id' => 3,
                'name'          => 'Job G',
                'description'   => 'job g descriptions',
                'status'        => 1,
                'reference'     => '3adeb619-f5a5-4fd0-8d0c-5d2f9ecf454b',
            ],
            [
                'id'            => 8,
                'created_by_id' => 3,
                'name'          => 'Job H',
                'description'   => 'job h descriptions',
                'status'        => 0,
                'reference'     => '7c5476c6-b804-4032-990f-6e97c7a9c857',
            ],
            [
                'id'            => 9,
                'created_by_id' => 4,
                'name'          => 'Job I',
                'description'   => 'job i descriptions',
                'status'        => 1,
                'reference'     => 'f35faa25-e2a6-4596-ac92-35f9679eb3d7',
            ],
            [
                'id'            => 10,
                'created_by_id' => 4,
                'name'          => 'Job J',
                'description'   => 'job j descriptions',
                'status'        => 2,
                'reference'     => '62b4a0a9-0a35-4c99-bc8a-ef25054fb718',
            ],
        ];

        $assignees = [
            [
                'job_id'  => 1,
                'user_id' => 3,
            ],
            [
                'job_id'  => 1,
                'user_id' => 4,
            ],
            [
                'job_id'  => 5,
                'user_id' => 2,
            ],
            [
                'job_id'  => 7,
                'user_id' => 1,
            ],
            [
                'job_id'  => 8,
                'user_id' => 2,
            ],
            [
                'job_id'  => 8,
                'user_id' => 3,
            ],
            [
                'job_id'  => 8,
                'user_id' => 1,
            ],
        ];
        $this->query('SET FOREIGN_KEY_CHECKS=0;');
        $this->table('jobs')->truncate();
        $this->table('assignees')->truncate();
        $this->table('authentication')->truncate();
        $this->table('users')->truncate();

        $this->table('users')->insert($users)->save();
        $this->table('authentication')->insert($authentication)->save();
        $this->table('jobs')->insert($jobs)->save();
        $this->table('assignees')->insert($assignees)->save();
        $this->query('SET FOREIGN_KEY_CHECKS=1;');
    }
}
