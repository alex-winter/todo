<?php

declare(strict_types=1);

namespace ToucandoTests\Unit;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Interfaces\RouteInterface;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Service\RespondInterface;
use Toucando\Service\UuidInterface;
use Toucando\Value\UuidInterface as UuidValue;
use Toucando\Persistence\JobInterface as JobEntity;
use Toucando\Service\JsonAdapter\JobInterface as JobJsonAdapter;
use Toucando\Service\JsonAdapter\UserInterface as UserJsonAdapter;
use Toucando\Service\Repository\JobInterface as JobRepository;
use Toucando\Service\Repository\UserInterface as UserRepository;

trait Mocks
{
    /**
     * @return UuidValue|MockObject
     */
    private function mockUuidValue(): UuidValue
    {
        return $this->createMock(UuidValue::class);
    }

    /**
     * @return UuidInterface|MockObject
     */
    private function mockUuidService(): UuidInterface
    {
        return $this->createMock(UuidInterface::class);
    }

    /**
     * @return UserEntity|MockObject
     */
    private function mockUserEntity(): UserEntity
    {
        return $this->createMock(UserEntity::class);
    }

    /**
     * @return JobEntity|MockObject
     */
    private function mockJobEntity(): JobEntity
    {
        return $this->createMock(JobEntity::class);
    }

    private function mockEntityManager(): EntityManagerInterface
    {
        return $this->createMock(EntityManagerInterface::class);
    }

    /**
     * @return UserRepository|MockObject
     */
    private function mockUserRepository(): UserRepository
    {
        return $this->createMock(UserRepository::class);
    }

    /**
     * @return JobRepository|MockObject
     */
    private function mockJobRepository(): JobRepository
    {
        return $this->createMock(JobRepository::class);
    }

    /**
     * @return UserJsonAdapter|MockObject
     */
    private function mockUserAdapter(): UserJsonAdapter
    {
        return $this->createMock(UserJsonAdapter::class);
    }

    /**
     * @return JobJsonAdapter|MockObject
     */
    private function mockJobAdapter(): JobJsonAdapter
    {
        return $this->createMock(JobJsonAdapter::class);
    }

    /**
     * @return RouteInterface|MockObject
     */
    private function mockRoute(): RouteInterface
    {
        return $this->createMock(RouteInterface::class);
    }

    /**
     * @return RespondInterface|MockObject
     */
    private function mockRespond(): RespondInterface
    {
        return $this->createMock(RespondInterface::class);
    }
}
