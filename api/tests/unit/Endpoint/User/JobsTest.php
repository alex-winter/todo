<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint\User;

use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\User\Jobs;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

/**
 * @covers \Toucando\Endpoint\User\Jobs
 */
final class JobsTest extends TestCase
{
    use Request, Mocks;

    public function testReturnsJobsAssignedAndJobsCreated(): void
    {
        $user = $this->mockUserEntity();

        $jobsAssigned  = [$this->mockJobEntity(), $this->mockJobEntity()];
        $jobsCreatedBy = [$this->mockJobEntity(), $this->mockJobEntity()];

        $jobsAssignedJson  = [['name' => 'Job 1'], ['name' => 'Job 2']];
        $jobsCreatedByJson = [['name' => 'Job 3'], ['name' => 'Job 4']];

        $jobRepository = $this->mockJobRepository();
        $jobRepository->expects($this->once())->method('fetchByAssignee')->with($user)->willReturn($jobsAssigned);
        $jobRepository->expects($this->once())->method('fetchByCreatedBy')->with($user)->willReturn($jobsCreatedBy);

        $jobJsonAdapter = $this->mockJobAdapter();
        $jobJsonAdapter
            ->expects($this->exactly(2))
            ->method('multipleToJson')
            ->withConsecutive(
                $jobsAssigned,
                $jobsCreatedBy
            )
            ->willReturnOnConsecutiveCalls(
                $jobsAssignedJson,
                $jobsCreatedByJson
            );

        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('success')
            ->with([
                'jobsAssigned' => $jobsAssignedJson,
                'jobsCreated'  => $jobsCreatedByJson,
            ]);

        $sut = new Jobs(
            $jobRepository,
            $respond,
            $jobJsonAdapter
        );

        $request = $this->makePostRequest()->withAttribute('logged-in-user', $user);

        $sut($request);
    }
}
