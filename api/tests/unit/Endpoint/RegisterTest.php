<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Endpoint;

use PHPUnit\Framework\TestCase;
use Toucando\Endpoint\Register;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;

/**
 * @covers \Toucando\Endpoint\Register
 */
final class RegisterTest extends TestCase
{
    use Request, Mocks;

    /**
     * @dataProvider provideFields
     */
    public function testAllFieldsMustBePassed(array $fields): void
    {
        $respond = $this->mockRespond();
        $respond->expects($this->once())->method('badRequest')->with(Register::ALL_FIELDS_MUST_BE_PASSED);

        $sut = new Register(
            $this->mockEntityManager(),
            $respond,
            $this->mockUuidService()
        );

        $request = $this->makePostRequest();
        $request->withParsedBody($fields);

        $sut($request);
    }

    public function provideFields(): array
    {
        return [
            [['username' => 'aaa', 'password' => 'aaa']],
            [['password' => 'aaa', 'passwordConfirmation' => 'aaa']],
            [['username' => 'aaa', 'passwordConfirmation' => 'aaa']],
        ];
    }

    public function testIfPasswordAndPasswordConfirmationDoNotMatch(): void
    {
        $respond = $this->mockRespond();
        $respond->expects($this->once())->method('badRequest')->with(Register::PASSWORD_DOES_NOT_MATCH_CONFIRMATION);

        $sut = new Register(
            $this->mockEntityManager(),
            $respond,
            $this->mockUuidService()
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody([
                'username'             => 'aaa',
                'password'             => 'aaa',
                'passwordConfirmation' => 'bbb',
            ]);

        $sut($request);
    }
}
