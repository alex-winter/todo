<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\JobInterface;
use ToucandoTests\Unit\Mocks;
use Toucando\Persistence\Job;

/**
 * @covers \Toucando\Persistence\Job
 */
final class JobTest extends TestCase
{
    use Mocks;

    public function testCanSetAndRetrieveCreatedBy(): void
    {
        $createdBy = $this->mockUserEntity();

        $sut = new Job($createdBy, '', $this->mockUuidValue());

        $this->assertSame($createdBy, $sut->getCreatedBy());
    }

    public function testCanSetAndRetrieveName(): void
    {
        $name = 'Alex Winter';

        $sut = new Job($this->mockUserEntity(), $name, $this->mockUuidValue());

        $this->assertSame($name, $sut->getName());
    }

    public function testCanSetAndRetrieveReference(): void
    {
        $uuid = '84798abb-c5a8-4837-bacc-2b56bf0a16e1';

        $reference = $this->mockUuidValue();
        $reference->expects($this->once())->method('getRaw')->willReturn($uuid);

        $sut = new Job($this->mockUserEntity(), '', $reference);

        $this->assertSame($uuid, $sut->getReference()->getRaw());
    }

    public function testCanSetAndRetrieveDescription(): void
    {
        $description = 'aaa';

        $sut = new Job($this->mockUserEntity(), '', $this->mockUuidValue(), $description);

        $this->assertSame($description, $sut->getDescription());
    }

    public function testCanSetAndRetrieveImageFilename(): void
    {
        $fileName = 'aaa.jpg';

        $sut = new Job($this->mockUserEntity(), '', $this->mockUuidValue(), '', $fileName);

        $this->assertSame($fileName, $sut->getImageFilename());
    }

    public function testCanGetDefaultStatus(): void
    {
        $sut = new Job($this->mockUserEntity(), '', $this->mockUuidValue());

        $this->assertSame(Job::STATUS_DEFAULT, $sut->getStatus());
    }

    public function testCanSetStatus(): void
    {
        $sut = new Job($this->mockUserEntity(), '', $this->mockUuidValue());

        $sut->setStatus(Job::STATUS_ARCHIVED);

        $this->assertSame(Job::STATUS_ARCHIVED, $sut->getStatus());
    }

    public function testCanSetAndRetrieveAssignees(): void
    {
        $sut = new Job($this->mockUserEntity(), '', $this->mockUuidValue());

        $userOne = $this->mockUserEntity();
        $userTwo = $this->mockUserEntity();

        $sut->addAssignee($userOne);
        $sut->addAssignee($userTwo);

        $this->assertSame([$userOne, $userTwo], $sut->getAssignees());
    }

    public function testCanArchive(): void
    {
        $sut = new Job($this->mockUserEntity(), '', $this->mockUuidValue());

        $sut->archive();

        $this->assertSame(JobInterface::STATUS_ARCHIVED, $sut->getStatus());
    }
}
