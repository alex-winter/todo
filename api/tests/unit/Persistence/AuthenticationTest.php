<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Persistence;

use PHPUnit\Framework\TestCase;
use ToucandoTests\Unit\Mocks;
use Toucando\Persistence\Authentication;

/**
 * @covers \Toucando\Persistence\Authentication
 */
final class AuthenticationTest extends TestCase
{
    use Mocks;

    public function testCanSetAndRetrieveUsername(): void
    {
        $username = 'alexwinter';

        $sut = new Authentication(
            $username,
            '$2y$10$LIUC8RE8BMb/15QP1t0q0.xHcugb7h3Y5YjpYoMfUyeqZjATPcuOe',
            $this->mockUserEntity()
        );

        $this->assertSame($username, $sut->getUsername());
    }

    public function testCanSetAndRetrievePassword(): void
    {
        $password = '$2y$10$LIUC8RE8BMb/15QP1t0q0.xHcugb7h3Y5YjpYoMfUyeqZjATPcuOe';

        $sut = new Authentication('alexwinter', $password, $this->mockUserEntity());

        $this->assertSame($password, $sut->getPassword());
    }

    public function testCanSetAndRetrieveUser(): void
    {
        $user = $this->mockUserEntity();

        $sut = new Authentication('alexwinter', '$2y$10$LIUC8RE8BMb/15QP1t0q0.xHcugb7h3Y5YjpYoMfUyeqZjATPcuOe', $user);

        $this->assertSame($user, $sut->getUser());
    }

    public function testCanSetCurrentToken(): void
    {
        $sut = new Authentication(
            'alexwinter',
            '$2y$10$LIUC8RE8BMb/15QP1t0q0.xHcugb7h3Y5YjpYoMfUyeqZjATPcuOe',
            $this->mockUserEntity()
        );

        $uuid = $this->mockUuidValue();
        $uuid->expects($this->once())->method('getRaw')->willReturn('c119d173-9cbc-4229-b302-8ad5e65806ab');

        $sut->setCurrentToken($uuid);
    }
}
