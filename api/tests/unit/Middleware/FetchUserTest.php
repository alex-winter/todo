<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware;

use PHPUnit\Framework\TestCase;
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;
use Toucando\Middleware\FetchUser;
use Toucando\Value\Uuid;

/**
 * @covers \Toucando\Middleware\FetchUser
 */
final class FetchUserTest extends TestCase
{
    use Request, Mocks;

    public function testFindUserReferenceInBodyOfRequest(): void
    {
        $uuid = 'f9d3bfff-8a57-4b49-8d86-43eaaaddd9a9';
        $user = $this->mockUserEntity();

        $repository = $this->mockUserRepository();
        $repository
            ->expects($this->once())
            ->method('fetchByReference')
            ->with(new Uuid($uuid))
            ->willReturn($user);

        $sut = new FetchUser(
            $this->mockRespond(),
            $repository
        );

        $request = $this
            ->makePostRequest()
            ->withParsedBody([
                'userReference' => $uuid
            ]);

        $next = function (SlimRequest $request) use ($user) {
            $this->assertSame($user, $request->getAttribute('logged-in-user'));

            return new Response();
        };

        $sut($request, new Response(), $next);
    }

    public function testFindUserReferenceInRouteParams(): void
    {
        $uuid = 'f9d3bfff-8a57-4b49-8d86-43eaaaddd9a9';
        $user = $this->mockUserEntity();

        $repository = $this->mockUserRepository();
        $repository
            ->expects($this->once())
            ->method('fetchByReference')
            ->with(new Uuid($uuid))
            ->willReturn($user);

        $sut = new FetchUser(
            $this->mockRespond(),
            $repository
        );

        $route = $this->mockRoute();
        $route
            ->expects($this->once())
            ->method('getArgument')
            ->with('user-reference')
            ->willReturn($uuid);

        $request = $this
            ->makePostRequest()
            ->withAttribute('route', $route);

        $next = function (SlimRequest $request) use ($user) {
            $this->assertSame($user, $request->getAttribute('logged-in-user'));

            return new Response();
        };

        $sut($request, new Response(), $next);
    }

    public function testIfUserReferenceIsNotPassedInRequest(): void
    {
        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('badRequest')
            ->with(FetchUser::USER_REFERENCE_MISSING);

        $sut = new FetchUser(
            $respond,
            $this->mockUserRepository()
        );

        $route = $this->mockRoute();
        $route
            ->expects($this->once())
            ->method('getArgument')
            ->with('user-reference')
            ->willReturn(null);

        $request = $this
            ->makePostRequest()
            ->withAttribute('route', $route);

        $sut($request, new Response(), function () {
            return new Response();
        });
    }
}
