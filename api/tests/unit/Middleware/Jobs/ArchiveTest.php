<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs;

use PHPUnit\Framework\TestCase;
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;
use Toucando\Middleware\Jobs\Archive;

/**
 * @covers \Toucando\Middleware\Jobs\Archive
 */
final class ArchiveTest extends TestCase
{
    use Request, Mocks;

    public function testJobIsArchived(): void
    {
        $sut = new Archive();

        $job = $this->mockJobEntity();

        $job->expects($this->once())->method('archive');

        $request = $this->makePostRequest()->withAttribute('job', $job);

        $next = function (SlimRequest $request) use ($job) {
            $this->assertSame($job, $request->getAttribute('job'));

            return new Response();
        };

        $sut($request, new Response(), $next);
    }
}
