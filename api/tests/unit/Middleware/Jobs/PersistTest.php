<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs;

use PHPUnit\Framework\TestCase;
use Slim\Http\Response;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;
use Toucando\Middleware\Jobs\Persist;

/**
 * @covers \Toucando\Middleware\Jobs\Persist
 */
final class PersistTest extends TestCase
{
    use Request, Mocks;

    public function testJobIsPersisted(): void
    {
        $job        = $this->mockJobEntity();
        $repository = $this->mockJobRepository();
        $repository
            ->expects($this->once())
            ->method('persist')
            ->with($job);

        $sut = new Persist($repository);

        $request = $this->makePostRequest()->withAttribute('job', $job);

        $sut($request, new Response(), function () {
            return new Response();
        });
    }
}
