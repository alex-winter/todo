<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs\Fetch;

use PHPUnit\Framework\TestCase;
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;
use Toucando\Middleware\Jobs\Fetch\Single;
use Toucando\Value\Uuid;

/**
 * @covers \Toucando\Middleware\Jobs\Fetch\Single
 */
final class SingleTest extends TestCase
{
    use Request, Mocks;

    public function testIfJobIsNotFound(): void
    {
        $jobReference = 'a7e4c324-98ef-4556-aa59-6f1aa4e9f45c';

        $repository = $this->mockJobRepository();
        $repository
            ->expects($this->once())
            ->method('fetchByReference')
            ->with(new Uuid($jobReference))
            ->willReturn(null);

        $respond = $this->mockRespond();
        $respond->expects($this->once())->method('notFound')->with(Single::JOB_NOT_FOUND);

        $sut = new Single(
            $repository,
            $respond
        );

        $route = $this->mockRoute();
        $route
            ->expects($this->once())
            ->method('getArgument')
            ->with('job-reference')
            ->willReturn($jobReference);

        $request = $this->makePostRequest()
            ->withAttribute('route', $route);

        $sut($request, new Response(), function () {
            return new Response();
        });
    }

    public function testIfJobIsFound(): void
    {
        $jobReference = 'a7e4c324-98ef-4556-aa59-6f1aa4e9f45c';

        $job = $this->mockJobEntity();

        $repository = $this->mockJobRepository();
        $repository
            ->expects($this->once())
            ->method('fetchByReference')
            ->with(new Uuid($jobReference))
            ->willReturn($job);

        $sut = new Single(
            $repository,
            $this->mockRespond()
        );

        $route = $this->mockRoute();
        $route
            ->expects($this->once())
            ->method('getArgument')
            ->with('job-reference')
            ->willReturn($jobReference);

        $request = $this->makePostRequest()
            ->withAttribute('route', $route);

        $next = function (SlimRequest $request) use ($job) {
            $this->assertSame($job, $request->getAttribute('job'));

            return new Response();
        };

        $sut($request, new Response(), $next);
    }
}
