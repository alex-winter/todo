<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Middleware\Jobs;

use PHPUnit\Framework\TestCase;
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response;
use ToucandoTests\Unit\Mocks;
use ToucandoTests\Unit\Traits\Request;
use Toucando\Middleware\Jobs\Hydrate;
use Toucando\Persistence\JobInterface as JobEntity;
use Toucando\Value\Uuid;

/**
 * @covers \Toucando\Middleware\Jobs\Hydrate
 */
final class HydrateTest extends TestCase
{
    use Request, Mocks;

    public function testIfNoNameIsSent(): void
    {
        $respond = $this->mockRespond();
        $respond
            ->expects($this->once())
            ->method('badRequest')
            ->with(Hydrate::NAME_IS_MANDATORY);

        $sut = new Hydrate(
            $respond,
            $this->mockUuidService()
        );

        $sut($this->makePostRequest(), new Response(), function () {
            return new Response();
        });
    }

    public function testCreatingNewJob(): void
    {
        $uuidService = $this->mockUuidService();
        $uuidService->expects($this->once())->method('generate')->willReturn(
            new Uuid('5bc59a97-d119-463d-8344-b4f52f3333b9')
        );

        $sut = new Hydrate(
            $this->mockRespond(),
            $uuidService
        );

        $user = $this->mockUserEntity();

        $request = $this->makePostRequest()
            ->withAttribute('logged-in-user', $user)
            ->withParsedBody([
                'name'        => 'Job A',
                'description' => 'Job A description',
            ]);

        $next = function (SlimRequest $request) {
            /** @var JobEntity $job */
            $job = $request->getAttribute('job');

            $this->assertInstanceOf(JobEntity::class, $job);

            $this->assertSame('Job A', $job->getName());
            $this->assertSame('Job A description', $job->getDescription());

            return new Response();
        };

        $sut($request, new Response(), $next);
    }
}
