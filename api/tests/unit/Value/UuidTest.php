<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Value;

use PHPUnit\Framework\TestCase;
use Toucando\Exception\InvalidValue;
use Toucando\Value\Uuid;

/**
 * @covers \Toucando\Value\Uuid
 */
final class UuidTest extends TestCase
{
    public function testInvalidValue(): void
    {
        $this->expectException(InvalidValue::class);

        new Uuid('this is not a uuid you plonker');
    }

    public function testCanSetAndRetrieveUuid(): void
    {
        $uuid = '17b87642-0bec-4506-8794-fe9e0bba84d9';

        $sut = new Uuid($uuid);

        $this->assertSame($uuid, $sut->getRaw());
    }
}
