<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Traits;

use Slim\Http\Environment;
use Slim\Http\Request as SlimRequest;

trait Request
{
    public function makePostRequest(): SlimRequest
    {
        return SlimRequest::createFromEnvironment(
            Environment::mock(['REQUEST_METHOD' => 'POST'])
        );
    }
}
