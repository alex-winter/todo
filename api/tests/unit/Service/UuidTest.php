<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Service;

use PHPUnit\Framework\TestCase;
use Toucando\Service\Uuid;

/**
 * @covers \Toucando\Service\Uuid
 */
final class UuidTest extends TestCase
{
    public function testReturnsAValidUuid(): void
    {
        $sut = new Uuid();

        $regex = '/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/';

        $this->assertRegExp($regex, $sut->generate()->getRaw());
    }
}
