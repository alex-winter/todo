<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Service;

use PHPUnit\Framework\TestCase;
use Toucando\Service\Respond;

/**
 * @covers \Toucando\Service\Respond
 */
final class RespondTest extends TestCase
{
    public function testSuccessResponseReturns200Code(): void
    {
        $sut = new Respond();

        $this->assertSame(200, $sut->success()->getStatusCode());
    }

    public function testUnauthorizedResponseReturns401Code(): void
    {
        $sut = new Respond();

        $this->assertSame(401, $sut->unauthorized('message')->getStatusCode());
    }

    public function testBadRequestResponseReturns400Code(): void
    {
        $sut = new Respond();

        $this->assertSame(400, $sut->badRequest('message')->getStatusCode());
    }

    public function testNotFoundResponseReturns404Code(): void
    {
        $sut = new Respond();

        $this->assertSame(404, $sut->notFound('message')->getStatusCode());
    }
}
