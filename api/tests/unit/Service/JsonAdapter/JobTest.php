<?php

declare(strict_types=1);

namespace ToucandoTests\Unit\Service\JsonAdapter;

use PHPUnit\Framework\TestCase;
use ToucandoTests\Unit\Mocks;
use Toucando\Service\JsonAdapter\Job;
use Toucando\Value\Uuid;

/**
 * @covers \Toucando\Service\JsonAdapter\Job
 */
final class JobTest extends TestCase
{
    use Mocks;

    public function testWillAdaptArrayFromJobEntity(): void
    {
        $sut = new Job($this->mockUserAdapter());

        $reference     = new Uuid('8e0de505-f706-4028-8c60-0ef20a57a813');
        $name          = 'Job A';
        $description   = 'description for Job A';
        $status        = 1;
        $imageFilename = 'image.jpg';
        $createdBy     = $this->mockUserEntity();
        $assignees     = [];

        $job = $this->mockJobEntity();
        $job->expects($this->once())->method('getReference')->willReturn($reference);
        $job->expects($this->once())->method('getName')->willReturn($name);
        $job->expects($this->once())->method('getDescription')->willReturn($description);
        $job->expects($this->once())->method('getStatus')->willReturn($status);
        $job->expects($this->once())->method('getImageFilename')->willReturn($imageFilename);
        $job->expects($this->once())->method('getCreatedBy')->willReturn($createdBy);
        $job->expects($this->once())->method('getAssignees')->willReturn($assignees);

        $array = $sut->toJson($job);

        $this->assertArrayHasKey('reference', $array);
        $this->assertArrayHasKey('name', $array);
        $this->assertArrayHasKey('description', $array);
        $this->assertArrayHasKey('status', $array);
        $this->assertArrayHasKey('imageFilename', $array);
        $this->assertArrayHasKey('createdBy', $array);
        $this->assertArrayHasKey('assignees', $array);

        $this->assertSame($reference->getRaw(), $array['reference']);
        $this->assertSame($name, $array['name']);
        $this->assertSame($description, $array['description']);
        $this->assertSame($status, $array['status']);
        $this->assertSame($imageFilename, $array['imageFilename']);
        $this->assertSame([], $array['createdBy']);
        $this->assertSame($assignees, $array['assignees']);
    }

    public function testWillAdaptFromJobEntities(): void
    {
        $sut = new Job($this->mockUserAdapter());

        $referenceOne     = new Uuid('8e0de505-f706-4028-8c60-0ef20a57a813');
        $nameOne          = 'Job A';
        $descriptionOne   = 'description for Job A';
        $statusOne        = 1;
        $imageFilenameOne = 'image.jpg';
        $createdByOne     = $this->mockUserEntity();
        $assigneesOne     = [];

        $referenceTwo     = new Uuid('8e0de505-f706-4028-8c60-0ef20a57a815');
        $nameTwo          = 'Job B';
        $descriptionTwo   = 'description for Job B';
        $statusTwo        = 2;
        $imageFilenameTwo = 'image2.jpg';
        $createdByTwo     = $this->mockUserEntity();
        $assigneesTwo     = [];

        $job = $this->mockJobEntity();
        $job->expects($this->once())->method('getReference')->willReturn($referenceOne);
        $job->expects($this->once())->method('getName')->willReturn($nameOne);
        $job->expects($this->once())->method('getDescription')->willReturn($descriptionOne);
        $job->expects($this->once())->method('getStatus')->willReturn($statusOne);
        $job->expects($this->once())->method('getImageFilename')->willReturn($imageFilenameOne);
        $job->expects($this->once())->method('getCreatedBy')->willReturn($createdByOne);
        $job->expects($this->once())->method('getAssignees')->willReturn($assigneesOne);

        $jobTwo = $this->mockJobEntity();
        $jobTwo->expects($this->once())->method('getReference')->willReturn($referenceTwo);
        $jobTwo->expects($this->once())->method('getName')->willReturn($nameTwo);
        $jobTwo->expects($this->once())->method('getDescription')->willReturn($descriptionTwo);
        $jobTwo->expects($this->once())->method('getStatus')->willReturn($statusTwo);
        $jobTwo->expects($this->once())->method('getImageFilename')->willReturn($imageFilenameTwo);
        $jobTwo->expects($this->once())->method('getCreatedBy')->willReturn($createdByTwo);
        $jobTwo->expects($this->once())->method('getAssignees')->willReturn($assigneesTwo);

        $array = $sut->multipleToJson($job, $jobTwo);

        $this->assertCount(2, $array);

        $this->assertArrayHasKey('reference', $array[0]);
        $this->assertArrayHasKey('name', $array[0]);
        $this->assertArrayHasKey('description', $array[0]);
        $this->assertArrayHasKey('status', $array[0]);
        $this->assertArrayHasKey('imageFilename', $array[0]);
        $this->assertArrayHasKey('createdBy', $array[0]);
        $this->assertArrayHasKey('assignees', $array[0]);

        $this->assertSame($referenceOne->getRaw(), $array[0]['reference']);
        $this->assertSame($nameOne, $array[0]['name']);
        $this->assertSame($descriptionOne, $array[0]['description']);
        $this->assertSame($statusOne, $array[0]['status']);
        $this->assertSame($imageFilenameOne, $array[0]['imageFilename']);
        $this->assertSame([], $array[0]['createdBy']);
        $this->assertSame($assigneesOne, $array[0]['assignees']);

        $this->assertSame($referenceTwo->getRaw(), $array[1]['reference']);
        $this->assertSame($nameTwo, $array[1]['name']);
        $this->assertSame($descriptionTwo, $array[1]['description']);
        $this->assertSame($statusTwo, $array[1]['status']);
        $this->assertSame($imageFilenameTwo, $array[1]['imageFilename']);
        $this->assertSame([], $array[1]['createdBy']);
        $this->assertSame($assigneesTwo, $array[1]['assignees']);
    }
}
