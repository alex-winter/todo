<?php

declare(strict_types=1);

namespace ToucandoTests\System;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Dotenv\Dotenv;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\Job;
use Toucando\Persistence\User;

trait Doctrine
{
    protected static $allEntities = [
        Authentication::class,
        User::class,
        Job::class,
    ];

    /** @var EntityManagerInterface */
    private static $entityManager;

    public static function setUpBeforeClass(): void
    {
        require __DIR__ . '/../../vendor/autoload.php';

        $environment = Dotenv::create(__DIR__ . '/../../');
        $environment->load();

        $settings = include __DIR__ . '/../../app/settings.php';
        $settings = $settings['settings']['doctrine'];

        self::$entityManager = EntityManager::create(
            $settings['connection'],
            Setup::createAnnotationMetadataConfiguration(
                $settings['meta']['entity_path'],
                $settings['meta']['auto_generate_proxies'],
                $settings['meta']['proxy_dir'],
                $settings['meta']['cache'],
                false
            )
        );

        static::clearDatabase();

        parent::setUpBeforeClass();
    }

    protected function tearDown(): void
    {
        self::clearDatabase();

        parent::setUp();
    }

    protected static function clearDatabase(): void
    {
        foreach (self::$allEntities as $class) {
            self::truncate($class);
        }
    }

    protected static function truncate(string $class): void
    {
        $entityManager = self::$entityManager;

        $connection = $entityManager->getConnection();
        $schema     = $connection->getSchemaManager();

        $query = '';

        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        foreach ($schema->listTableNames() as $tableName) {
            $query .= 'TRUNCATE `' . $tableName . '`;';
        }
        $connection->executeQuery($query);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }
}
