<?php

declare(strict_types=1);

namespace ToucandoTests\System\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\Job;
use Toucando\Persistence\User;
use Toucando\Value\Uuid;
use ToucandoTests\System\Doctrine;

final class JobTest extends TestCase
{
    use Doctrine;

    public function testCanPersistAndRetrieveJobFromTheDatabase(): void
    {
        $createdBy     = new User(new Uuid('a75302ca-ddf5-4b64-b901-308489380932'));
        $name          = 'Job A';
        $reference     = new Uuid('1d8042cf-5376-419a-8dfc-81d8a5b57601');
        $description   = 'job description for Job A';
        $imageFilename = 'image.jpg';

        $sut = new Job($createdBy, $name, $reference, $description, $imageFilename);

        self::$entityManager->persist($createdBy);
        self::$entityManager->persist($sut);
        self::$entityManager->flush();

        /** @var Job $persisted */
        $persisted = self::$entityManager
            ->getRepository(Job::class)
            ->findOneBy(['reference' => $sut->getReference()->getRaw()]);

        $this->assertSame($createdBy->getReference()->getRaw(), $persisted->getCreatedBy()->getReference()->getRaw());
        $this->assertSame($name, $persisted->getName());
        $this->assertSame($reference->getRaw(), $persisted->getReference()->getRaw());
        $this->assertSame($description, $persisted->getDescription());
        $this->assertSame($imageFilename, $persisted->getImageFilename());
    }

    public function testUpdateJob(): void
    {
        $createdBy     = new User(new Uuid('a75302ca-ddf5-4b64-b901-308489380932'));
        $name          = 'Job A';
        $reference     = new Uuid('1d8042cf-5376-419a-8dfc-81d8a5b57601');
        $description   = 'job description for Job A';
        $imageFilename = 'image.jpg';

        $sut = new Job($createdBy, $name, $reference, $description, $imageFilename);

        self::$entityManager->persist($createdBy);
        self::$entityManager->persist($sut);
        self::$entityManager->flush();

        /** @var Job $persisted */
        $persisted = self::$entityManager
            ->getRepository(Job::class)
            ->findOneBy(['reference' => $sut->getReference()->getRaw()]);

        $updatedName          = 'new name';
        $updatedImageFilename = 'new-image.jpg';
        $updatedDescription   = 'new description';
        $updatedStatus        = Job::STATUS_ARCHIVED;
        $assignee             = new User(new Uuid('fc509925-a20f-47c0-85c4-bd259aeae3ce'));

        $persisted->setStatus($updatedStatus);
        $persisted->setName($updatedName);
        $persisted->setImageFilename($updatedImageFilename);
        $persisted->setDescription($updatedDescription);
        $persisted->addAssignee($assignee);

        self::$entityManager->persist($assignee);
        self::$entityManager->persist($persisted);
        self::$entityManager->flush();

        /** @var Job $updated */
        $updated = self::$entityManager
            ->getRepository(Job::class)
            ->findOneBy(['reference' => $persisted->getReference()->getRaw()]);

        $this->assertSame($updatedName, $updated->getName());
        $this->assertSame($updatedImageFilename, $updated->getImageFilename());
        $this->assertSame($updatedDescription, $updated->getDescription());
        $this->assertSame($updatedStatus, $updated->getStatus());
        $this->assertSame($assignee->getReference()->getRaw(), $updated->getAssignees()[0]->getReference()->getRaw());
    }

    public function testArchiveJobPersistsToDatabase(): void
    {
        $createdBy = new User(new Uuid('a75302ca-ddf5-4b64-b901-308489380932'));

        $sut = new Job(
            $createdBy,
            'Job A',
            new Uuid('1d8042cf-5376-419a-8dfc-81d8a5b57601'),
            'job description for Job A',
            'image.jpg'
        );

        self::$entityManager->persist($createdBy);
        self::$entityManager->persist($sut);
        self::$entityManager->flush();

        /** @var Job $persisted */
        $persisted = self::$entityManager
            ->getRepository(Job::class)
            ->findOneBy(['reference' => $sut->getReference()->getRaw()]);

        $persisted->archive();

        self::$entityManager->persist($persisted);
        self::$entityManager->flush();

        /** @var Job $updated */
        $updated = self::$entityManager
            ->getRepository(Job::class)
            ->findOneBy(['reference' => $persisted->getReference()->getRaw()]);

        $this->assertSame(Job::STATUS_ARCHIVED, $updated->getStatus());
    }
}
