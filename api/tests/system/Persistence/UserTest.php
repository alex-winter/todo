<?php

declare(strict_types=1);

namespace ToucandoTests\System\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\User;
use Toucando\Value\Uuid;
use ToucandoTests\System\Doctrine;

final class UserTest extends TestCase
{
    use Doctrine;

    public function testCanPersistUserToTheDatabase(): void
    {
        $reference = new Uuid('12642ba8-3092-4d24-b05c-2d70ce6520f0');

        $user = new User(
            $reference
        );

        self::$entityManager->persist($user);
        self::$entityManager->flush();

        /** @var User $user */
        $user = self::$entityManager->find(User::class, $user->getId());

        $this->assertInstanceOf(User::class, $user);
        $this->assertSame($reference->getRaw(), $user->getReference()->getRaw());
    }
}
