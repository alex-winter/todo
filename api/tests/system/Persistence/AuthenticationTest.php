<?php

declare(strict_types=1);

namespace ToucandoTests\System\Persistence;

use PHPUnit\Framework\TestCase;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\User;
use Toucando\Value\Uuid;
use ToucandoTests\System\Doctrine;

final class AuthenticationTest extends TestCase
{
    use Doctrine;

    public function testCanPersistNewEntityAndRetrieveThatEntityFromTheDatabase(): void
    {
        $user = new User(new Uuid('7e37d5a7-df6a-4413-804d-68408b62eca4'));

        $username    = 'usera';
        $rawPassword = 'letmein';
        $password    = password_hash($rawPassword, PASSWORD_BCRYPT);

        $authentication = new Authentication(
            $username,
            $password,
            $user
        );

        self::$entityManager->persist($user);
        self::$entityManager->persist($authentication);
        self::$entityManager->flush();

        /** @var Authentication $persisted */
        $persisted = self::$entityManager
            ->getRepository(Authentication::class)
            ->findOneBy(['username' => $username]);

        $this->assertSame($username, $persisted->getUsername());
        $this->assertSame($password, $persisted->getPassword());
        $this->assertTrue(password_verify($rawPassword, $persisted->getPassword()));
    }

    public function testCanSetToken(): void
    {
        $user = new User(new Uuid('7e37d5a7-df6a-4413-804d-68408b62eca4'));

        $authentication = new Authentication(
            'usera',
            password_hash('aaa', PASSWORD_BCRYPT),
            $user
        );

        self::$entityManager->persist($user);
        self::$entityManager->persist($authentication);
        self::$entityManager->flush();

        /** @var Authentication $persisted */
        $persisted = self::$entityManager
            ->getRepository(Authentication::class)
            ->findOneBy(['username' => 'usera']);

        $uuid = '1325cd0e-208c-49e3-bf39-411a8254b62e';

        $persisted->setCurrentToken(new Uuid($uuid));

        self::$entityManager->persist($persisted);
        self::$entityManager->flush();

        /** @var Authentication $foundByToken */
        $foundByToken = self::$entityManager
            ->getRepository(Authentication::class)
            ->findOneBy(['currentToken' => $uuid]);

        $this->assertInstanceOf(Authentication::class, $foundByToken);
    }
}
