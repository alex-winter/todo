<?php

declare(strict_types=1);

namespace Toucando;

use Doctrine\ORM\EntityManagerInterface;
use Toucando\Middleware;
use Toucando\Service;

/**
 * @property Middleware\FetchAssignees         $middlewareFetchAssignees
 * @property Middleware\FetchUser              $middlewareFetchLoggedInUser
 * @property Middleware\Jobs\Archive           $middlewareJobsArchive
 * @property Middleware\Jobs\Fetch\Single      $middlewareJobsFetchSingle
 * @property Middleware\Jobs\Hydrate           $middlewareJobsHydrate
 * @property Middleware\Jobs\Persist           $middlewareJobsPersist
 *
 * @property EntityManagerInterface            $databaseTodo
 * @property Service\JsonAdapter\JobInterface  $jsonAdapterJob
 * @property Service\JsonAdapter\UserInterface $jsonAdapterUser
 * @property Service\Repository\JobInterface   $repositoryJob
 * @property Service\Repository\UserInterface  $repositoryUser
 * @property Service\RespondInterface          $respond
 * @property Service\UuidInterface             $uuid
 */
final class Container extends \Slim\Container
{
    public function __set(string $name, $value)
    {
        $this[$name] = $value;
    }
}
