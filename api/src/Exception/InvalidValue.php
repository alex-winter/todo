<?php

namespace Toucando\Exception;

/**
 * Type exception.
 */
class InvalidValue extends \InvalidArgumentException
{
}
