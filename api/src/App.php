<?php

declare(strict_types=1);

namespace Toucando;

final class App extends \Slim\App
{
    /** @var Container */
    private $container;

    public function __construct(Container $container)
    {
        parent::__construct($container);

        $this->container = $container;
    }

    public function getContainer(): Container
    {
        return $this->container;
    }
}
