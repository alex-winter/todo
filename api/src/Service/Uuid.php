<?php

declare(strict_types=1);

namespace Toucando\Service;

use Ramsey\Uuid\Uuid as Ramsey;
use Toucando\Value\Uuid as UuidValue;

final class Uuid implements UuidInterface
{
    public function generate(): UuidValue
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return new UuidValue(
            Ramsey::uuid4()->serialize()
        );
    }
}
