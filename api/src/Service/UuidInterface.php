<?php

declare(strict_types=1);

namespace Toucando\Service;

use Toucando\Value\Uuid;

interface UuidInterface
{
    public function generate(): Uuid;
}
