<?php

declare(strict_types=1);

namespace Toucando\Service\JsonAdapter;

use Toucando\Persistence\UserInterface as Entity;

interface UserInterface
{
    public function toJson(Entity $user): array;

    public function multipleToJson(Entity ...$users): array;
}
