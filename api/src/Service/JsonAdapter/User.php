<?php

declare(strict_types=1);

namespace Toucando\Service\JsonAdapter;

use Toucando\Persistence\UserInterface as EntityInterface;

final class User implements UserInterface
{
    public function toJson(EntityInterface $user): array
    {
        return [
            'reference' => $user->getReference()->getRaw(),
            'username'  => $user->getAuthentication()->getUsername(),
        ];
    }

    public function multipleToJson(EntityInterface ...$users): array
    {
        $adapter = function (EntityInterface $user): array {
            return $this->toJson($user);
        };

        return array_map($adapter, $users);
    }
}
