<?php

declare(strict_types=1);

namespace Toucando\Service\JsonAdapter;

use Toucando\Persistence\JobInterface as Entity;

interface JobInterface
{
    public function toJson(Entity $job): array;

    public function multipleToJson(Entity ...$jobs): array;
}
