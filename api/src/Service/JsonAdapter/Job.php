<?php

declare(strict_types=1);

namespace Toucando\Service\JsonAdapter;

use Toucando\Persistence\JobInterface as EntityInterface;

final class Job implements JobInterface
{
    /** @var UserInterface */
    private $userAdapter;

    public function __construct(UserInterface $userAdapter)
    {
        $this->userAdapter = $userAdapter;
    }

    public function toJson(EntityInterface $job): array
    {
        return [
            'reference'     => $job->getReference()->getRaw(),
            'name'          => $job->getName(),
            'description'   => $job->getDescription(),
            'status'        => $job->getStatus(),
            'imageFilename' => $job->getImageFilename(),
            'createdBy'     => $this->userAdapter->toJson($job->getCreatedBy()),
            'assignees'     => $this->userAdapter->multipleToJson(...$job->getAssignees()),
        ];
    }

    public function multipleToJson(EntityInterface ...$jobs): array
    {
        $adapter = function (EntityInterface $job): array {
            return $this->toJson($job);
        };

        return array_map($adapter, $jobs);
    }
}
