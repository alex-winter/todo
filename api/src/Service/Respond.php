<?php

declare(strict_types=1);

namespace Toucando\Service;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;

final class Respond implements RespondInterface
{
    public function success(array $data = []): ResponseInterface
    {
        $response = new Response();

        $data = [
            'code'    => 200,
            'success' => true,
            'data'    => $data,
        ];

        return $response->withJson($data, 200);
    }

    public function unauthorized(string $message): ResponseInterface
    {
        $response = new Response();

        $data = [
            'code'    => 401,
            'success' => false,
            'message' => $message,
        ];

        return $response->withJson($data, 401);
    }

    public function badRequest(string $message): ResponseInterface
    {
        $response = new Response();

        $data = [
            'code'    => 400,
            'success' => false,
            'message' => $message
        ];

        return $response->withJson($data, 400);
    }

    public function notFound(string $message): ResponseInterface
    {
        $response = new Response();

        $data = [
            'code'    => 404,
            'success' => false,
            'message' => $message
        ];

        return $response->withJson($data, 404);
    }

    public function serverError(string $message): ResponseInterface
    {
        $response = new Response();

        $data = [
            'code'    => 500,
            'success' => false,
            'message' => $message,
        ];

        return $response->withJson($data, 500);
    }
}
