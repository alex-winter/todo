<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Toucando\Persistence\JobInterface as Entity;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Value\Uuid;

interface JobInterface
{
    public function fetchByReference(Uuid $reference): ?Entity;

    public function persist(Entity $job): void;

    public function fetchByAssignee(UserEntity $user): array;

    public function fetchByCreatedBy(UserEntity $user): array;
}
