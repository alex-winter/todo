<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Toucando\Persistence\User as Entity;
use Toucando\Persistence\UserInterface as EntityInterface;
use Toucando\Value\Uuid;

final class User implements UserInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityInterface[]
     */
    public function fetchByMultipleReference(Uuid ...$references): array
    {
        $references = array_map(function (Uuid $uuid): string {
            return $uuid->getRaw();
        }, $references);

        /** @var Entity[] $users */
        $users = $this->entityManager
            ->createQueryBuilder()
            ->select('u')
            ->from(Entity::class, 'u')
            ->where('u.reference IN (:references)')
            ->setParameter('references', $references)
            ->getQuery()
            ->getResult();

        return $users;
    }

    public function fetchByReference(Uuid $reference): EntityInterface
    {
        /** @var Entity $user */
        $user = $this->entityManager
            ->getRepository(Entity::class)
            ->findOneBy(['reference' => $reference->getRaw()]);

        return $user;
    }
}
