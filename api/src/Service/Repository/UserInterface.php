<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Toucando\Persistence\UserInterface as Entity;
use Toucando\Value\Uuid;

interface UserInterface
{
    /**
     * @return Entity[]
     */
    public function fetchByMultipleReference(Uuid ...$references): array;

    public function fetchByReference(Uuid $reference): Entity;
}
