<?php

declare(strict_types=1);

namespace Toucando\Service\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Toucando\Persistence\Job as Entity;
use Toucando\Persistence\JobInterface as EntityInterface;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Value\Uuid;

final class Job implements JobInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function fetchByReference(Uuid $reference): ?EntityInterface
    {
        /** @var Entity|null $job */
        $job = $this->entityManager
            ->getRepository(Entity::class)
            ->findOneBy(['reference' => $reference->getRaw()]);

        return $job;
    }

    /**
     * @return Entity[]
     */
    public function fetchByAssignee(UserEntity $user): array
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('j')
            ->from(Entity::class, 'j')
            ->where(':id MEMBER OF j.assignees')
            ->setParameter('id', $user->getId())
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Entity[]
     */
    public function fetchByCreatedBy(UserEntity $user): array
    {
        /** @var Entity[] $jobs */
        $jobs = $this->entityManager
            ->getRepository(Entity::class)
            ->findBy(['createdBy' => $user->getId()]);

        return $jobs;
    }

    public function persist(EntityInterface $job): void
    {
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }
}
