<?php

declare(strict_types=1);

namespace Toucando\Service;

use Psr\Http\Message\ResponseInterface;

interface RespondInterface
{
    public function success(array $data = []): ResponseInterface;

    public function unauthorized(string $message): ResponseInterface;

    public function badRequest(string $message): ResponseInterface;

    public function notFound(string $message): ResponseInterface;
}
