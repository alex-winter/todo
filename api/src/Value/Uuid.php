<?php

namespace Toucando\Value;

use Toucando\Exception\InvalidValue;

final class Uuid implements UuidInterface
{
    private $uuid;

    public function __construct(string $uuid)
    {
        $uuid = strtolower($uuid);

        if (!preg_match(self::PATTERN, $uuid)) {
            throw new InvalidValue('Invalid UUID string');
        }

        $this->uuid = $uuid;
    }

    public function getRaw(): string
    {
        return $this->uuid;
    }
}
