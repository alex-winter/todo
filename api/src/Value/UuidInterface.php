<?php

declare(strict_types=1);

namespace Toucando\Value;

interface UuidInterface
{
    public const PATTERN = '/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/D';

    public function getRaw(): string;
}
