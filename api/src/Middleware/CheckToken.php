<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Service\RespondInterface;

final class CheckToken
{
    /** @var EntityManagerInterface */
    private $database;

    /** @var RespondInterface */
    private $respond;

    public function __construct(EntityManagerInterface $database, RespondInterface $respond)
    {
        $this->database = $database;
        $this->respond  = $respond;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        /** @var UserEntity $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        $token = $request->getHeader('X-Token');

        if ($token === null) {
            return $this->respond->unauthorized('Invalid token');
        }

        $authentication = $this->database
            ->createQueryBuilder()
            ->select('count(a.id)')
            ->from(Authentication::class, 'a')
            ->leftJoin('a.user', 'u')
            ->where('a.currentToken = :token')
            ->andWhere('u.id = :userId')
            ->setParameters([
                'token'  => $token,
                'userId' => $loggedInUser->getId(),
            ])
            ->getQuery()
            ->getSingleScalarResult();

        if ($authentication < 1) {
            return $this->respond->unauthorized('User is not authenticated');
        }

        return $next($request, $response);
    }
}
