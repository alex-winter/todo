<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class Cor
{
    public const ALLOWED_HEADERS   = 'Content-Type, Accept, Origin, Authorization, X-Token';
    public const ALLOWED_METHODS   = 'GET, POST, PATCH, OPTIONS';
    public const ALLOW_CREDENTIALS = 'true';

    public function __invoke(RequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
    {
        /** @var ResponseInterface $nextResponse */
        $nextResponse = $next($request, $response);

        return $nextResponse
            ->withHeader('Access-Control-Allow-Origin', getenv('ALLOW_ORIGIN'))
            ->withHeader('Access-Control-Allow-Headers', self::ALLOWED_HEADERS)
            ->withHeader('Access-Control-Allow-Methods', self::ALLOWED_METHODS)
            ->withHeader('Access-Control-Allow-Credentials', self::ALLOW_CREDENTIALS);
    }
}
