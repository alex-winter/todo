<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Service\Repository\UserInterface;
use Toucando\Value\Uuid;

final class FetchAssignees
{
    /** @var UserInterface */
    private $userRepository;

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $body = $request->getParsedBody();

        $references = $body['assignee-references'] ?? [];

        $references = array_map(function (string $reference) {
            return new Uuid($reference);
        }, $references);

        $assignees = $this->userRepository->fetchByMultipleReference(...$references);

        return $next(
            $request->withAttribute('assignees', $assignees),
            $response
        );
    }
}
