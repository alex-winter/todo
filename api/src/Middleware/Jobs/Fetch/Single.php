<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs\Fetch;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteInterface;
use Toucando\Persistence\JobInterface;
use Toucando\Service\Repository\JobInterface as JobRepository;
use Toucando\Service\RespondInterface;
use Toucando\Value\Uuid;

final class Single
{
    public const JOB_NOT_FOUND = 'Job was found';

    /** @var JobRepository */
    private $jobRepository;

    /** @var RespondInterface */
    private $respond;

    public function __construct(JobRepository $jobRepository, RespondInterface $respond)
    {
        $this->jobRepository = $jobRepository;
        $this->respond       = $respond;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        /** @var RouteInterface $route */
        $route = $request->getAttribute('route');

        $reference = $route->getArgument('job-reference');

        $job = $this->jobRepository->fetchByReference(
            new Uuid($reference)
        );

        if (!($job instanceof JobInterface)) {
            $this->respond->notFound(self::JOB_NOT_FOUND);
        }

        return $next(
            $request->withAttribute('job', $job),
            $response
        );
    }
}
