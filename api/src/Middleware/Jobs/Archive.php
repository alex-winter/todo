<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\JobInterface as JobEntity;

final class Archive
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        /** @var JobEntity $job */
        $job = $request->getAttribute('job');

        $job->archive();

        return $next(
            $request->withAttribute('job', $job),
            $response
        );
    }
}
