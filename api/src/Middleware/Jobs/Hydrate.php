<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\Job;
use Toucando\Persistence\JobInterface;
use Toucando\Service\RespondInterface;
use Toucando\Service\UuidInterface;

final class Hydrate
{
    public const NAME_IS_MANDATORY = 'name is mandatory can not be blank';

    /** @var RespondInterface */
    private $respond;

    /** @var UuidInterface */
    private $uuid;

    public function __construct(RespondInterface $respond, UuidInterface $uuid)
    {
        $this->respond = $respond;
        $this->uuid    = $uuid;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $createdBy = $request->getAttribute('logged-in-user');
        $job       = $request->getAttribute('job');

        $body = $request->getParsedBody();

        $name = $body['name'] ?? '';

        if ($name === '') {
            return $this->respond->badRequest(self::NAME_IS_MANDATORY);
        }

        $description = $body['description'] ?? '';
        $status      = $body['status'] ?? JobInterface::STATUS_DEFAULT;

        if ($job instanceof JobInterface) {
            $job->setName($name);
            $job->setDescription($description);
            $job->setStatus($status);
        } else {
            $job = new Job(
                $createdBy,
                $name,
                $this->uuid->generate(),
                $description
            );
        }

        return $next(
            $request->withAttribute('job', $job),
            $response
        );
    }
}
