<?php

declare(strict_types=1);

namespace Toucando\Middleware\Jobs;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\JobInterface as JobEntity;
use Toucando\Service\Repository\JobInterface;

final class Persist
{
    /** @var JobInterface */
    private $jobRepository;

    public function __construct(JobInterface $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        /** @var JobEntity $job */
        $job = $request->getAttribute('job');

        $this->jobRepository->persist($job);

        return $next($request, $response);
    }
}
