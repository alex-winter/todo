<?php

declare(strict_types=1);

namespace Toucando\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteInterface;
use Toucando\Service\Repository\UserInterface as UserRepository;
use Toucando\Service\RespondInterface;
use Toucando\Value\Uuid;

final class FetchUser
{
    public const USER_REFERENCE_MISSING = 'user reference missing from request';

    /** @var RespondInterface */
    private $respond;

    /** @var UserRepository */
    private $userRepository;

    public function __construct(RespondInterface $respond, UserRepository $userRepository)
    {
        $this->respond        = $respond;
        $this->userRepository = $userRepository;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $body = $request->getParsedBody();

        $reference = $body['userReference'] ?? null;

        if ($reference === null) {
            /** @var RouteInterface $route */
            $route = $request->getAttribute('route');

            $reference = $route->getArgument('user-reference');
        }

        if ($reference === null) {
            return $this->respond->badRequest(self::USER_REFERENCE_MISSING);
        }

        $user = $this->userRepository->fetchByReference(
            new Uuid($reference)
        );

        return $next(
            $request->withAttribute('logged-in-user', $user),
            $response
        );
    }
}
