<?php

declare(strict_types=1);

namespace Toucando\Persistence\Traits;

use Toucando\Value\Uuid;
use Toucando\Value\UuidInterface;

trait Reference
{
    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="guid", length=36, nullable=false)
     */
    private $reference;

    public function getReference(): UuidInterface
    {
        return new Uuid($this->reference);
    }
}
