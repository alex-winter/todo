<?php

declare(strict_types=1);

namespace Toucando\Persistence\Traits;

trait Id
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
}
