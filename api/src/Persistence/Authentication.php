<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Persistence\Traits\Reference;
use Toucando\Value\UuidInterface as UuidValue;

/**
 * @ORM\Entity
 * @ORM\Table(name="authentication", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class Authentication implements AuthenticationInterface
{
    use Id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="current_token", type="string", nullable=true)
     */
    private $currentToken;

    /**
     * @var UserInterface
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="authentication")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct(string $username, string $password, UserInterface $user)
    {
        $this->username = $username;
        $this->password = $password;
        $this->user     = $user;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setCurrentToken(UuidValue $token): void
    {
        $this->currentToken = $token->getRaw();
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }
}
