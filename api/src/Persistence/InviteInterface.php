<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface;

interface InviteInterface
{
    public function getMessage(): string;

    public function getSender(): UserInterface;

    public function getReference(): UuidInterface;
}
