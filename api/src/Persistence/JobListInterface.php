<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface;

interface JobListInterface
{
    public function getReference(): UuidInterface;

    /**
     * @return UserInterface[]
     */
    public function getUsers(): array;
}
