<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Persistence\Traits\Reference;
use Toucando\Value\UuidInterface as UuidValueInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="users", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class User implements UserInterface
{
    use Id, Reference;

    /**
     * @var JobInterface[]
     *
     * @ORM\OneToMany(targetEntity="Job", mappedBy="createdBy")
     */
    private $jobs;

    /**
     * @var AuthenticationInterface
     *
     * @ORM\OneToOne(targetEntity="Authentication", mappedBy="user")
     */
    private $authentication;

    /**
     * @ORM\OneToMany(targetEntity="Invite", mappedBy="sender")
     */
    private $invitationsSent;

    /**
     * @ORM\OneToMany(targetEntity="Invite", mappedBy="invitee")
     */
    private $invitationsReceived;

    /**
     * @ORM\ManyToMany(targetEntity="JobList", inversedBy="users")
     * @ORM\JoinTable(name="users_lists")
     */
    private $jobLists;

    public function __construct(UuidValueInterface $reference)
    {
        $this->reference           = $reference->getRaw();
        $this->jobs                = new ArrayCollection();
        $this->invitationsSent     = new ArrayCollection();
        $this->invitationsReceived = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function addInvitation(Invite $invite): void
    {
        $this->invitationsSent->add($invite);
    }

    /**
     * @return Invite[]
     */
    public function getInvitationsSent(): array
    {
        return $this->invitationsSent->getValues();
    }

    /**
     * @return Invite[]
     */
    public function getInvitationsReceived(): array
    {
        return $this->invitationsReceived->getValues();
    }

    public function getAuthentication(): AuthenticationInterface
    {
        return $this->authentication;
    }

    /**
     * @return JobList
     */
    public function getJobLists()
    {
        return $this->jobLists->getValues();
    }
}
