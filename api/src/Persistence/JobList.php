<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Persistence\Traits\Reference;
use Toucando\Value\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="job_lists", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
final class JobList
{
    use Id, Reference;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="lists")
     */
    private $users;

    public function __construct(UuidInterface $reference)
    {
        $this->reference = $reference->getRaw();
        $this->users = new ArrayCollection();
    }

    /**
     * @return UserInterface[]
     */
    public function getUsers(): array
    {
        $this->users->getValues();
    }
}
