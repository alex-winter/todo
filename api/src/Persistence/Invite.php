<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Persistence\Traits\Reference;
use Toucando\Value\Uuid;
use Toucando\Value\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="invites", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
final class Invite implements InviteInterface
{
    use Id, Reference;

    /**
     * @var UserInterface
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $sender;

    /**
     * @var UserInterface
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     * @ORM\JoinColumn(name="invitee_id", referencedColumnName="id")
     */
    private $invitee;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var int
     * @ORM\Column(name="seen", type="boolean")
     */
    private $seen = 0;

    public function __construct(Uuid $reference, UserInterface $sender, UserInterface $invitee, string $message = '')
    {
        $this->reference = $reference->getRaw();
        $this->sender    = $sender;
        $this->invitee   = $invitee;
        $this->message   = $message;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getSender(): UserInterface
    {
        return $this->sender;
    }
}
