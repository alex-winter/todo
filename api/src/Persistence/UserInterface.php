<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Toucando\Value\UuidInterface as UuidValueInterface;

interface UserInterface
{
    public function getId(): int;

    public function getReference(): UuidValueInterface;

    public function addInvitation(Invite $invite): void;

    /**
     * @return Invite[]
     */
    public function getInvitationsSent(): array;

    /**
     * @return Invite[]
     */
    public function getInvitationsReceived(): array;

    public function getAuthentication(): AuthenticationInterface;
}
