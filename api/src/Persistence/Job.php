<?php

declare(strict_types=1);

namespace Toucando\Persistence;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Toucando\Persistence\Traits\Id;
use Toucando\Persistence\Traits\Reference;
use Toucando\Value\UuidInterface as UuidValueInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="jobs", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 *
 * @uses ORM\Table
 */
class Job implements JobInterface
{
    use Id, Reference;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image_filename", type="string")
     */
    private $imageFilename;

    /**
     * @var UserInterface
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="jobs")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(
     *     name="assignees",
     *     joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $assignees;

    public function __construct(
        UserInterface $createdBy,
        string $name,
        UuidValueInterface $reference,
        string $description = '',
        string $imageFilename = ''
    ) {
        $this->name          = $name;
        $this->description   = $description;
        $this->imageFilename = $imageFilename;
        $this->createdBy     = $createdBy;
        $this->status        = self::STATUS_DEFAULT;
        $this->reference     = $reference->getRaw();
        $this->assignees     = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getImageFilename(): string
    {
        return $this->imageFilename;
    }

    public function getCreatedBy(): UserInterface
    {
        return $this->createdBy;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return UserInterface[]
     */
    public function getAssignees(): array
    {
        return $this->assignees->getValues();
    }

    public function addAssignee(UserInterface $user): void
    {
        $this->assignees->add($user);
    }

    public function archive(): void
    {
        $this->status = self::STATUS_ARCHIVED;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function setImageFilename(string $imageFilename): void
    {
        $this->imageFilename = $imageFilename;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
