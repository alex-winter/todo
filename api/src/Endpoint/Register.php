<?php

declare(strict_types=1);

namespace Toucando\Endpoint;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\User;
use Toucando\Service\RespondInterface;
use Toucando\Service\UuidInterface;

final class Register
{
    public const ALL_FIELDS_MUST_BE_PASSED            = 'All fields must be passed';
    public const PASSWORD_DOES_NOT_MATCH_CONFIRMATION = 'Password does not match confirmation';
    public const USERNAME_ALREADY_EXISTS              = 'username already exists';

    /** @var EntityManagerInterface */
    private $database;

    /** @var RespondInterface */
    private $respond;

    /** @var UuidInterface */
    private $uuid;

    public function __construct(EntityManagerInterface $database, RespondInterface $respond, UuidInterface $uuid)
    {
        $this->database = $database;
        $this->respond  = $respond;
        $this->uuid     = $uuid;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();

        if (!isset($body['username'], $body['password'], $body['passwordConfirmation'])) {
            return $this->respond->badRequest(self::ALL_FIELDS_MUST_BE_PASSED);
        }

        $username     = $body['username'];
        $password     = $body['password'];
        $confirmation = $body['passwordConfirmation'];

        if ($password !== $confirmation) {
            return $this->respond->badRequest(self::PASSWORD_DOES_NOT_MATCH_CONFIRMATION);
        }

        $password = password_hash($password, PASSWORD_BCRYPT);

        $existingUsername = $this->database
            ->getRepository(Authentication::class)
            ->findBy(['username' => $username]);

        if (count($existingUsername) > 0) {
            return $this->respond->badRequest(self::USERNAME_ALREADY_EXISTS);
        }

        $user           = new User($this->uuid->generate());
        $authentication = new Authentication($username, $password, $user);

        $this->database->persist($user);
        $this->database->persist($authentication);
        $this->database->flush();

        return $this->respond->success();
    }
}
