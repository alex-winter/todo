<?php

declare(strict_types=1);

namespace Toucando\Endpoint\User;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\UserInterface as UserEntity;
use Toucando\Service\JsonAdapter\JobInterface as JobJsonAdapter;
use Toucando\Service\Repository\JobInterface as JobRepository;
use Toucando\Service\RespondInterface;

final class Jobs
{
    /** @var JobRepository */
    private $jobRepository;

    /** @var RespondInterface */
    private $respond;

    /** @var JobJsonAdapter */
    private $jobJsonAdapter;

    public function __construct(JobRepository $jobRepository, RespondInterface $respond, JobJsonAdapter $jobJsonAdapter)
    {
        $this->jobRepository  = $jobRepository;
        $this->respond        = $respond;
        $this->jobJsonAdapter = $jobJsonAdapter;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        /** @var UserEntity $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        $jobsByAssignee = $this->jobRepository->fetchByAssignee($loggedInUser);
        $jobsCreatedBy  = $this->jobRepository->fetchByCreatedBy($loggedInUser);

        return $this->respond->success([
            'jobsAssigned' => $this->jobJsonAdapter->multipleToJson(...$jobsByAssignee),
            'jobsCreated'  => $this->jobJsonAdapter->multipleToJson(...$jobsCreatedBy),
        ]);
    }
}
