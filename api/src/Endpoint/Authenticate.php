<?php

declare(strict_types=1);

namespace Toucando\Endpoint;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\AuthenticationInterface;
use Toucando\Service\RespondInterface;
use Toucando\Service\JsonAdapter\UserInterface;
use Toucando\Service\UuidInterface;

final class Authenticate
{
    /** @var RespondInterface */
    private $respond;

    /** @var UserInterface */
    private $responseAdapterUser;

    /** @var UuidInterface */
    private $uuid;

    /** @var EntityManagerInterface */
    private $database;

    public function __construct(
        RespondInterface $respond,
        UserInterface $responseAdapterUser,
        UuidInterface $uuid,
        EntityManagerInterface $database
    ) {
        $this->respond             = $respond;
        $this->responseAdapterUser = $responseAdapterUser;
        $this->uuid                = $uuid;
        $this->database            = $database;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();

        if (!isset($body['username'], $body['password'])) {
            return $this->respond->unauthorized('unauthorized');
        }

        $username = $body['username'];
        $password = $body['password'];

        $authentication = $this->database
            ->getRepository(Authentication::class)
            ->findBy(['username' => $username]);

        if (count($authentication) < 1) {
            return $this->respond->unauthorized('unauthorized');
        }

        /** @var AuthenticationInterface $authentication */
        $authentication = $authentication[0];

        $correctPassword = password_verify($password, $authentication->getPassword());

        if (!$correctPassword) {
            return $this->respond->unauthorized('unauthorized');
        }

        $token = $this->uuid->generate();

        $authentication->setCurrentToken($token);

        $this->database->persist($authentication);
        $this->database->flush();

        return $this->respond->success([
            'token' => $token->getRaw(),
            'user'  => $this->responseAdapterUser->toJson($authentication->getUser()),
        ]);
    }
}
