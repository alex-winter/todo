<?php

declare(strict_types=1);

use Toucando\Service\Uuid;

$app->getContainer()->uuid = function () {
    return new Uuid();
};
