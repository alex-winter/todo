<?php

declare(strict_types=1);

use Toucando\Container;
use Toucando\Service\Repository\User;
use Toucando\Service\Repository\UserInterface;

$app->getContainer()->repositoryUser = function (Container $container): UserInterface {
    return new User(
        $container->databaseTodo
    );
};
