<?php

declare(strict_types=1);

use Toucando\Container;
use Toucando\Service\Repository\Job;
use Toucando\Service\Repository\JobInterface;

$app->getContainer()->repositoryJob = function (Container $container): JobInterface {
    return new Job(
        $container->databaseTodo
    );
};
