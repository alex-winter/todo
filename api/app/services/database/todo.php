<?php

declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Toucando\Container;

$app->getContainer()->databaseTodo = function (Container $container): EntityManagerInterface {
    $settings = $container->settings;
    $settings = $settings['doctrine'];

    return EntityManager::create(
        $settings['connection'],
        Setup::createAnnotationMetadataConfiguration(
            $settings['meta']['entity_path'],
            $settings['meta']['auto_generate_proxies'],
            $settings['meta']['proxy_dir'],
            $settings['meta']['cache'],
            false
        )
    );
};
