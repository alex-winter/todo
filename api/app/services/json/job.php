<?php

declare(strict_types=1);

use Toucando\Container;
use Toucando\Service\JsonAdapter\Job;
use Toucando\Service\JsonAdapter\JobInterface;

$app->getContainer()->jsonAdapterJob = function (Container $container): JobInterface {
    return new Job(
        $container->jsonAdapterUser
    );
};
