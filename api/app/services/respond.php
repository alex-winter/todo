<?php

declare(strict_types=1);

use Toucando\Service\Respond;
use Toucando\Service\RespondInterface;

$app->getContainer()->respond = function (): RespondInterface {
    return new Respond();
};
