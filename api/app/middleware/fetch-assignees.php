<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Middleware\FetchAssignees;

$app->getContainer()->middlewareFetchAssignees = function (Container $container): callable {
    return function (
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) use ($container) : ResponseInterface {
        $fetchAssignees = new FetchAssignees(
            $container->repositoryUser
        );

        return $fetchAssignees($request, $response, $next);
    };
};
