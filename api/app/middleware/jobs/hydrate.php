<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Middleware\Jobs\Hydrate;

$app->getContainer()->middlewareJobsHydrate = function (Container $container): callable {
    return function (
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) use ($container) : ResponseInterface {
        $hydrate = new Hydrate(
            $container->respond,
            $container->uuid
        );

        return $hydrate($request, $response, $next);
    };
};
