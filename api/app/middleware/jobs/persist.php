<?php

declare(strict_types=1);

use Toucando\Container;
use Toucando\Middleware\Jobs\Persist;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

$app->getContainer()->middlewareJobsPersist = function (Container $container): callable {
    return function (
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) use ($container) : ResponseInterface {
        $persist = new Persist(
            $container->repositoryJob
        );

        return $persist($request, $response, $next);
    };
};
