<?php

declare(strict_types=1);

use Toucando\Container;
use Toucando\Middleware\Jobs\Fetch\Single;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

$app->getContainer()->middlewareJobsFetchSingle = function (Container $container): callable {
    return function (
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ) use ($container) : ResponseInterface {
        $fetchSingle = new Single(
            $container->repositoryJob,
            $container->respond
        );

        return $fetchSingle($request, $response, $next);
    };
};
