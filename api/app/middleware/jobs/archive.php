<?php

declare(strict_types=1);

use Toucando\Middleware\Jobs\Archive;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

$app->getContainer()->middlewareJobsArchive = function (): callable {
    return function (
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $delete = new Archive();

        return $delete($request, $response, $next);
    };
};
