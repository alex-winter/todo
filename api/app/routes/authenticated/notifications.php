<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Persistence\UserInterface;

$app->get(
    '/user/{user-reference}/notifications',
    function (ServerRequestInterface $request) {
        /** @var Container $this */

        /** @var UserInterface $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        $notifications = [];

        $invitations = $loggedInUser->getInvitationsReceived();

        foreach ($invitations as $invite) {
            $notifications[] = [
                'type'     => 'invite',
                'username' => $invite->getSender()->getAuthentication()->getUsername(),
            ];
        }

        return $this->respond->success([
            'notifications' => $notifications
        ]);
    }
);
