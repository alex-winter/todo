<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Persistence\JobInterface as JobEntity;

$app
    ->post(
        '/job/create',
        function (ServerRequestInterface $request): ResponseInterface {
            /** @var Container $this */

            /** @var JobEntity $job */
            $job = $request->getAttribute('job');

            return $this->respond->success([
                'job' => $this->jsonAdapterJob->toJson($job)
            ]);
        }
    )
    ->setName('job.create')
    ->add($app->getContainer()->middlewareJobsPersist)
    ->add($app->getContainer()->middlewareJobsHydrate)
    ->add($app->getContainer()->middlewareFetchAssignees);
