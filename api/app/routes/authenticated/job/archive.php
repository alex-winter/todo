<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Toucando\Container;

$app
    ->patch(
        '/job/{job-reference}/archive',
        function (): ResponseInterface {
            /** @var Container $this */
            return $this->respond->success();
        }
    )
    ->setName('job.archive')
    ->add($app->getContainer()->middlewareJobsPersist)
    ->add($app->getContainer()->middlewareJobsArchive)
    ->add($app->getContainer()->middlewareJobsFetchSingle);
