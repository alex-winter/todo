<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteInterface;
use Toucando\Container;
use Toucando\Persistence\InviteInterface;
use Toucando\Persistence\UserInterface;

$app->post(
    '/user/{user-reference}/invitation/{invitation-reference}/accept',
    function (ServerRequestInterface $request) {
        /** @var Container $this */

        /** @var UserInterface $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        /** @var RouteInterface $route */
        $route = $request->getAttribute('route');

        $reference = $route->getArgument('invitation-reference');

        $invitations = $loggedInUser->getInvitationsReceived();

        $filter = array_filter(
            $invitations,
            function (InviteInterface $invite) use ($reference) {
                return $invite->getReference()->getRaw() === $reference;
            }
        );

        if (empty($filter)) {
            return $this->respond->notFound('Invite not found');
        }

        $this->databaseTodo->beginTransaction();

        try {
            /** @var InviteInterface $invite */
            $invite = $filter[0];

            $invite->accepted();

            $this->databaseTodo->persist($invite);



            $this->databaseTodo->flush();
            $this->databaseTodo->commit();
        } catch (Exception $exception) {
            $this->databaseTodo->rollback();

            return $this->respond->badRequest('persistence-error');
        }

        return $this->respond->success();
    }
);
