<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Persistence\Invite;
use Toucando\Persistence\User;
use Toucando\Value\Uuid;

$app->post(
    '/invitation/send',
    function (ServerRequestInterface $request) {
        /** @var Container $this */

        /** @var User $loggedInUser */
        $loggedInUser = $request->getAttribute('logged-in-user');

        $body = $request->getParsedBody();

        $references = $body['references'] ?? null;

        if ($references === null) {
            return $this->respond->badRequest('No user references given');
        }

        $message = $body['message'] ?? '';

        $users = $this->repositoryUser->fetchByMultipleReference(...array_map(function (string $reference) {
            return new Uuid($reference);
        }, $references));

        $invite = new Invite(
            $this->uuid->generate(),
            $loggedInUser,
            $users[0],
            $message
        );

        $this->databaseTodo->persist($invite);
        $this->databaseTodo->flush();

        return $this->respond->success();
    }
);
