<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface;
use Toucando\Persistence\Authentication;
use Toucando\Persistence\User;

$app->get(
    '/user/{user-reference}/invitation/usernames',
    function (ServerRequestInterface $request) {
        /** @var \Toucando\Container $this */

        /** @var User $user */
        $user = $request->getAttribute('logged-in-user');

        $usernames = $this->databaseTodo
            ->createQueryBuilder()
            ->select('a.username, u.reference')
            ->from(Authentication::class, 'a')
            ->leftJoin('a.user', 'u')
            ->where('a.user != :userId')
            ->setParameter('userId', $user->getId())
            ->getQuery()
            ->getArrayResult();

        return $this->respond->success([
            'usernames' => $usernames
        ]);
    }
);
