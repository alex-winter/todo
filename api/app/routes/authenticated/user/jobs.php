<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Endpoint\User\Jobs;

$app
    ->get(
        '/user/{user-reference}/jobs',
        function (ServerRequestInterface $request): ResponseInterface {
            /** @var Container $this */

            $jobs = new Jobs(
                $this->repositoryJob,
                $this->respond,
                $this->jsonAdapterJob
            );

            return $jobs($request);
        }
    )
    ->setName('user.jobs');
