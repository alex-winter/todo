<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Endpoint\Register;

$app
    ->post(
        '/register',
        function (ServerRequestInterface $request): ResponseInterface {
            /** @var Container $this */

            $register = new Register(
                $this->databaseTodo,
                $this->respond,
                $this->uuid
            );

            return $register($request);
        }
    );
