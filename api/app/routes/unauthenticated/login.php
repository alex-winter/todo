<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\Container;
use Toucando\Endpoint\Authenticate;

$app
    ->post(
        '/login',
        function (ServerRequestInterface $request): ResponseInterface {
            /** @var Container $this */

            $authenticate = new Authenticate(
                $this->respond,
                $this->jsonAdapterUser,
                $this->uuid,
                $this->databaseTodo
            );

            return $authenticate($request);
        }
    );
