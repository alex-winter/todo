<?php

declare(strict_types=1);

use Dotenv\Dotenv;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Toucando\App;
use Toucando\Container;
use Toucando\Middleware\CheckToken;
use Toucando\Middleware\Cor;
use Toucando\Middleware\FetchUser;

require_once __DIR__ . '/../vendor/autoload.php';

$bootstrap = function (): App {
    date_default_timezone_set('UTC');

    $environmentFile = Dotenv::create(__DIR__ . '/../');
    $environmentFile->load();

    $settings  = require __DIR__ . '/settings.php';
    $container = new Container($settings);
    $app       = new App($container);

    $load = function (string $root) use (&$load, &$app, &$container): void {
        if (is_file($root) && pathinfo($root, PATHINFO_EXTENSION) === 'php') {
            /** @noinspection PhpIncludeInspection */
            require_once $root;
        }

        if (is_dir($root)) {
            foreach (glob($root . '/*') as $path) {
                $load($path);
            }
        }
    };

    $load(__DIR__ . '/services');
    $load(__DIR__ . '/middleware');

    $app->options(
        '/{routes:.+}',
        function (
            /**@noinspection PhpUnusedParameterInspection */
            ServerRequestInterface $request,
            ResponseInterface $response,
            array $arguments
        ): ResponseInterface {
            return $response;
        }
    );

    /**
     * Before any code is executed check authenticity of cross domain request
     */
    $app->add(
        new Cor()
    );

    $load(__DIR__ . '/routes/unauthenticated');

    $app
        ->group('', function () use ($load) {
            $load(__DIR__ . '/routes/authenticated');
        })
        ->add(
            new CheckToken(
                $container->databaseTodo,
                $container->respond
            )
        )
        ->add(
            new FetchUser(
                $container->respond,
                $container->repositoryUser
            )
        );

    return $app;
};
