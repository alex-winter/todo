<?php

declare(strict_types=1);

require_once __DIR__ . '/../app/bootstrap.php';

/** @var \Toucando\App $app */
$app = $bootstrap();

/** @noinspection PhpUnhandledExceptionInspection */
$app->run();
