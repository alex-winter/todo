<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190719210111 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE invites (id INT AUTO_INCREMENT NOT NULL, sender_id INT DEFAULT NULL, invitee_id INT DEFAULT NULL, message LONGTEXT NOT NULL, seen INT NOT NULL, INDEX IDX_37E6A6CF624B39D (sender_id), INDEX IDX_37E6A6C7A512022 (invitee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jobs (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, image_filename VARCHAR(255) NOT NULL, status INT NOT NULL, reference CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', INDEX IDX_A8936DC5B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE assignees (job_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_12C41EDCBE04EA9 (job_id), INDEX IDX_12C41EDCA76ED395 (user_id), PRIMARY KEY(job_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, reference CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE authentication (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, current_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_FEB4C9FDA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE invites ADD CONSTRAINT FK_37E6A6CF624B39D FOREIGN KEY (sender_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE invites ADD CONSTRAINT FK_37E6A6C7A512022 FOREIGN KEY (invitee_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE jobs ADD CONSTRAINT FK_A8936DC5B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE assignees ADD CONSTRAINT FK_12C41EDCBE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id)');
        $this->addSql('ALTER TABLE assignees ADD CONSTRAINT FK_12C41EDCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE authentication ADD CONSTRAINT FK_FEB4C9FDA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assignees DROP FOREIGN KEY FK_12C41EDCBE04EA9');
        $this->addSql('ALTER TABLE invites DROP FOREIGN KEY FK_37E6A6CF624B39D');
        $this->addSql('ALTER TABLE invites DROP FOREIGN KEY FK_37E6A6C7A512022');
        $this->addSql('ALTER TABLE jobs DROP FOREIGN KEY FK_A8936DC5B03A8386');
        $this->addSql('ALTER TABLE assignees DROP FOREIGN KEY FK_12C41EDCA76ED395');
        $this->addSql('ALTER TABLE authentication DROP FOREIGN KEY FK_FEB4C9FDA76ED395');
        $this->addSql('DROP TABLE invites');
        $this->addSql('DROP TABLE jobs');
        $this->addSql('DROP TABLE assignees');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE authentication');
    }
}
