<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190723193345 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE job_lists (id INT AUTO_INCREMENT NOT NULL, reference CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_lists (user_id INT NOT NULL, joblist_id INT NOT NULL, INDEX IDX_EF513BECA76ED395 (user_id), INDEX IDX_EF513BEC9499DC89 (joblist_id), PRIMARY KEY(user_id, joblist_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_lists ADD CONSTRAINT FK_EF513BECA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_lists ADD CONSTRAINT FK_EF513BEC9499DC89 FOREIGN KEY (joblist_id) REFERENCES job_lists (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE phinxlog');
        $this->addSql('ALTER TABLE invites ADD reference CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', CHANGE seen seen TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_lists DROP FOREIGN KEY FK_EF513BEC9499DC89');
        $this->addSql('CREATE TABLE phinxlog (version BIGINT NOT NULL, migration_name VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, start_time DATETIME DEFAULT NULL, end_time DATETIME DEFAULT NULL, breakpoint TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(version)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE job_lists');
        $this->addSql('DROP TABLE users_lists');
        $this->addSql('ALTER TABLE invites DROP reference, CHANGE seen seen INT NOT NULL');
    }
}
